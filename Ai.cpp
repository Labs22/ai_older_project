#include <iostream>
#include <string>
#include <ctime>
#include <vector>
#include <ctime>
#include <cstdlib>

const int MAX_RESP = 3;

typedef std::vector<std::string> vstring;

vstring find_match(std::string input);
void copy(char *array[], vstring &v);


typedef struct {
    char *input;
    char *responses[MAX_RESP];
}record;

record KnowledgeBase[] = {
    {"Put your question",
    {"Put your answer...",
     "Put your second answer....",
     "Put your last answer...?"}
};

size_t nKnowledgeBaseSize = sizeof(KnowledgeBase)/sizeof(KnowledgeBase[0]);


int main() {
    srand((unsigned) time(NULL));

    std::string sInput = "";
    std::string sResponse = "";

    while(1) {
        std::cout << ">";
        std::getline(std::cin, sInput);
        vstring responses = find_match(sInput);
        if(sInput == "BYE") {
            std::cout << "IT WAS NICE TALKING TO YOU USER, SEE YOU NEXTTIME!" << std::endl;
            break;
        }
        else if(responses.size() == 0)  {
            std::cout << "Nao tenho conhecimento suficiente para responder." << std::endl;
        }
        else {
            int nSelection = rand()  % MAX_RESP;
            sResponse =   responses[nSelection]; std::cout << sResponse << std::endl;
        }
    }

    return 0;
}

// make a  search for the  user's input
// inside the database of the program
vstring find_match(std::string  input) {
    vstring result;
    for(int i = 0; i < nKnowledgeBaseSize;  ++i) {
        if(std::string(KnowledgeBase[i].input) == input) {
            copy(KnowledgeBase[i].responses, result);
            return result;
        }
    }
    return result;
}

void copy(char  *array[], vstring &v) {
    for(int i = 0;  i < MAX_RESP; ++i) {
        v.push_back(array[i]);
    }
}